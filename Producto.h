#ifndef ProductoH
#define ProductoH

class Producto {
private:
    int codigo;
    char nombre[50];
    float precio;
    char estado;
    
public:
    //Constructores
    Producto();
    Producto(int codigo, char nombre[], float precio, char estado);
    
    //Destructor
    ~Producto();
    
    //Metodos
    void cargarDatos();
    void mostrarDatos();
    
    void setCodigo(int nuevoCodigo);
    int getCodigo();
    
    void setNombre(char nuevoNombre[]);
    char* getNombre();
    
    void setPrecio(float nuevoPrecio);
    float getPrecio();
    
    void setEstado(char nuevoEstado);
    char getEstado();
};

#endif
