#include <iostream>
#include <string.h>
#include "Producto.h"

using namespace std;

Producto::Producto()
{
    codigo = 0;
    strcpy(nombre, " ");
    precio = 0.0;
    estado = ' ';
}

Producto::Producto(int codigo, char nombre[], float precio, char estado)
{
    this->codigo = codigo;
    strcpy(this->nombre, nombre);
    this->precio = precio;
    this->estado = tolower(estado);
}

Producto::~Producto()
{
    cout << "Objeto destruido." << endl;
}

void Producto::cargarDatos()
{
    cout << "Introduce los datos:" << endl;
    cout << "\tCodigo: "; cin >> codigo; cin.ignore();
    cout << "\tNombre: "; cin.getline(nombre, 49, '\n');
    cout << "\tPrecio: "; cin >> precio; cin.ignore();
    //cout << "\tActivo? s/n: "; ((tolower(cin.get()) == 's') ? true : false); cin.sync();
    cout << "\tActivo? s/n: "; cin.get(estado);
}

void Producto::mostrarDatos()
{
    cout << "Estos son los datos que tengo:" << endl;
    cout << "\tCodigo: " << codigo << endl;
    cout << "\tNombre: " << nombre << endl;
    cout << "\tPrecio: " << precio << endl;
    if(estado == 's')
        cout << "\tProducto activo" << endl;
    else
        cout << "\tProducto inactivo" << endl;
}

void Producto::setCodigo(int nuevoCodigo)
{
    codigo = nuevoCodigo;
}

int Producto::getCodigo()
{
    return codigo;
}

void Producto::setNombre(char nuevoNombre[])
{
    strcpy(nombre, nuevoNombre);
}

char* Producto::getNombre()
{
    return nombre;
}

void Producto::setPrecio(float nuevoPrecio)
{
    precio = nuevoPrecio;
}

float Producto::getPrecio()
{
    return precio;
}

void Producto::setEstado(char nuevoEstado)
{
    estado = nuevoEstado;
}

char Producto::getEstado()
{
    return estado;
}
