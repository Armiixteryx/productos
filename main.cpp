#include <iostream>
#include "Producto.h"
#include "Ingrediente.h"

using namespace std;

void pausar();

int main()
{
    /*
    
    //Producto tomate(1442, "Tomate", 400, true);
    //tomate.mostrarDatos();
    
    Producto tomate;
    tomate.cargarDatos();
    tomate.mostrarDatos();
    pausar();
    
    */
    
    Ingrediente nuez;
    Ingrediente azucar(2133, "La Casta", "Kg", 123000.32, 145003.43);
    azucar.mostrarDatos();
    
    Ingrediente *sal = new Ingrediente;
    sal->cargarDatos();
    sal->calcularPrecio();
    sal->mostrarDatos();
    
    Ingrediente *queso = new Ingrediente(3155, "Mozarella", "g", 200000, 250000.67);
    queso->mostrarDatos();
    
    
    Ingrediente **almacen = new Ingrediente * [10];
    for(int i=0; i<10; i++)
        almacen[i] = new Ingrediente;
    
    int aux;
    char resp;
    
    while(resp == 's' && aux < 10) {
        almacen[aux]->cargarDatos();
        aux++;
    }
    
    for(int i=0; i<aux; i++) {
        almacen[i]->mostrarDatos();
    }
    
    for(int i=0; i<aux; i++) {
        almacen[i]->calcularPrecio();
    }
    
    cout << "Codigo" << '\t';
    cout << "Nombre" << '\t';
    cout << "Medida" << '\t';
    cout << "Costo" << '\t';
    cout << "Precio" << '\t';
    cout << endl;
    
    for(int i=0; i<aux; i++) {
        almacen[i]->mostDatosLineal();
    }

    return 0;
}

void pausar()
{
    cout << "Presione ENTER para continuar...";
    cin.ignore(); cin.get(); cin.ignore();
}
