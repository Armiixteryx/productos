#include <iostream>
#include <string.h>
#include "Ingrediente.h"

using namespace std;

Ingrediente::Ingrediente()
{
    codigo = 0;
    strcpy(nombre, " ");
    strcpy(medida, " ");
    costo = 0.0;
    precio = 0.0;
}

Ingrediente::Ingrediente(int codigo, char nombre[], char medida[], float costo, float precio)
{
    this->codigo = codigo;
    strcpy(this->nombre, nombre);
    strcpy(this->medida, medida);
    this->costo = costo;
    this->precio = precio;
}

Ingrediente::~Ingrediente()
{
    cout << "Objeto Ingrediente destruido" << endl;
}

void Ingrediente::cargarDatos()
{
    cout << "Introduce los datos:" << endl;
    cout << "\tCodigo: "; cin >> codigo; cin.ignore();
    cout << "\tNombre: "; cin.getline(nombre, 49, '\n');
    cout << "\tMedida: "; cin.getline(medida, 9, '\n');
    cout << "\tCosto: "; cin >> costo; cin.ignore();
    precio = 0.0;
}

void Ingrediente::mostrarDatos()
{
    cout << "Estos son los datos que tengo:" << endl;
    cout << "\tCodigo: " << codigo << endl;
    cout << "\tNombre: " << nombre << endl;
    cout << "\tMedida: " << medida << endl;
    cout << "\tCosto: " << costo << endl;
    cout << "\tPrecio: " << precio << endl;
}

void Ingrediente::mostDatosLineal()
{
    cout << codigo << '\t';
    cout << nombre << '\t';
    cout << medida << '\t';
    cout << costo << '\t';
    cout << precio << '\t';
    cout << endl;
}

void Ingrediente::calcularPrecio()
{
    float ganancia = 40.0;
    float iva = 12.0;
    precio = (costo % ganancia) + (costo % iva);
}

void Ingrediente::setCodigo(int nuevoCodigo)
{
    codigo = nuevoCodigo;
}

int Ingrediente::getCodigo()
{
    return codigo;
}

void Ingrediente::setNombre(char nuevoNombre[])
{
    strcpy(nombre, nuevoNombre);
}

char* Ingrediente::getNombre()
{
    return nombre;
}

void Ingrediente::setMedida(char nuevoMedida[])
{
    strcpy(medida, nuevoMedida);
}

char* Ingrediente::getMedida()
{
    return medida;
}

void Ingrediente::setCosto(float nuevoCosto)
{
    costo = nuevoCosto;
}

float Ingrediente::getCosto()
{
    return costo;
}

void Ingrediente::setPrecio(float nuevoPrecio)
{
    precio = nuevoPrecio;
}

float Ingrediente::getPrecio()
{
    return precio;
}
