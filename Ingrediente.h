#ifndef IngredienteH
#define IngredienteH

class Ingrediente {
private:
    int codigo;
    char nombre[50];
    char medida[10];
    float costo;
    float precio;
    
public:
    Ingrediente();
    Ingrediente(int, char [], char [], float, float);
    ~Ingrediente();
    
    void cargarDatos();
    void mostrarDatos();
    void mostDatosLineal();
    void calcularPrecio();
    
    void setCodigo(int nuevoCodigo);
    int getCodigo();
    
    void setNombre(char nuevoNombre[]);
    char* getNombre();
    
    void setMedida(char nuevoMedida[]);
    char* getMedida();
    
    void setCosto(float nuevoCosto);
    float getCosto();
    
    void setPrecio(float nuevoPrecio);
    float getPrecio();
};

#endif
